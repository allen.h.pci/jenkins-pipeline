# Jenkins Pipeline
Repository holding the code to bring up a reproducible Jenkins pipeline. The idea is to use [packer](https://www.packer.io/) and [ansible](https://www.ansible.com/overview/how-ansible-works) to create pre-baked images as [AMIs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html), and then have [terraform](https://www.terraform.io/) provision the cloud resources on [AWS](https://aws.amazon.com/what-is-aws/). [Python](https://www.python.org/about/) is used for the main shell script since it's compatible with different platforms.


## Getting started
1. Download and install the prerequisites on your computer:
   1. [python](https://wiki.python.org/moin/BeginnersGuide/Download)
   2. [aws](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
   3. [packer](https://learn.hashicorp.com/tutorials/packer/get-started-install-cli)
   4. [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
   5. [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
2. Clone this repository onto your computer with something like <code>git clone git@gitlab.com:allen.h.pci/jenkins-pipeline.git</code> or <code>git clone https://gitlab.com/allen.h.pci/jenkins-pipeline.git</code>.
3. Create an AWS access key at the [Security Credentials](https://console.aws.amazon.com/iam/home#/security_credentials) page.
4. Create a [shared credentials file](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/create-shared-credentials-file.html) on your local computer.
5. Create a <code>ips.auto.tfvars</code> file in the <code>terraform</code> directory with the following content (replace the angle brackets):
```
TEAM_IP_ADDRESSES = [
  "<CIDR block>",  # <Team member name>
  "<CIDR block>",  # <Team member name>
]
```
6. Create a <code>.env</code> file in the <code>ansible/jenkins-controller/jenkins-jcasc</code> directory with the following content (replace the angle brackets):
```
JENKINS_ADMIN_ID=<replace this and angle brackets>
JENKINS_ADMIN_PASSWORD=<replace this and angle brackets>
```
7. Using the terminal, change the working directory to the project by running something like <code>cd jenkins-pipeline</code>.
8. Run <code>python3 main.py</code> to set up the pipeline.


##### Note
* Not finished :(
* If cost is an issue, make sure to de-register older AMIs and delete snapshots no longer in use, since these take up storage and the script isn't set up to do so automatically.
* There is a limit of 5 maximum VPCs per AWS region.
