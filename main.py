#!/usr/bin/env python3
import os
import subprocess
from uuid import uuid4

BREAK_ON_NONZERO_EXIT_CODE = True
# Set the AWS region here
AWS_REGION = 'us-west-1'


def main():
  # Create unique identifier for Jenkins worker AMI
  jenkins_worker_ami_build_uuid = str(uuid4())

  # Initial environment variables
  set_var(name='AWS_REGION', val=AWS_REGION)
  set_var(name='jenkins_worker_ami_build_uuid', val=jenkins_worker_ami_build_uuid)

  # Create snapshots and AMIs
  subprocess.run(
    'packer init ./packer',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )
  subprocess.run(
    'packer validate ./packer',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )
  subprocess.run(
    'packer build -only=jenkins-worker-build* ./packer',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )
  jenkins_worker_ami_id = subprocess.run(
    f"aws --region {AWS_REGION} ec2 describe-images --filters Name=tag:build_uuid,Values={jenkins_worker_ami_build_uuid} --query 'Images[0].ImageId' --output text",
    capture_output=True,
    text=True,
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  ).stdout
  set_var(name='JENKINS_WORKER_AMI_ID', val=jenkins_worker_ami_id)  # needed for Jenkins controller
  subprocess.run(
    'packer build -only=jenkins-controller-build* ./packer',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )

  # Provision resources on AWS
  subprocess.run(
    'terraform -chdir=terraform init',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )
  subprocess.run(
    'terraform -chdir=terraform validate',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )
  subprocess.run(
    'terraform -chdir=terraform apply -auto-approve',
    shell=True,
    check=BREAK_ON_NONZERO_EXIT_CODE
  )

  # Take down resources on AWS (snapshots and AMIs will be left over)
  # subprocess.run(
  #   'terraform -chdir=terraform destroy -auto-approve',
  #   shell=True,
  #   check=BREAK_ON_NONZERO_EXIT_CODE
  # )


def set_var(name, val):
  """Sets the environment variable for both Packer and Terraform to use."""
  os.environ[f'PKR_VAR_{name}'] = val
  os.environ[f'TF_VAR_{name}'] = val


if __name__ == '__main__':
  main()
