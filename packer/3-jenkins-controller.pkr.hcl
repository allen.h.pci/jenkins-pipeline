variable "jenkins_worker_ami_id" {
  description = "Environment variable that needs to be set; the worker AMI id"
  type        = string
  default     = ""
}

source "amazon-ebs" "jenkins-controller" {
  ami_name      = "jenkins-controller-${formatdate("YYYYMMDD-hhmmss", timestamp())}"
  instance_type = "t2.micro"
  region        = "${var.AWS_REGION}"
  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm*gp2"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  ssh_username = "ec2-user"
}

build {
  name = "jenkins-controller-build"
  sources = [
    "source.amazon-ebs.jenkins-controller"
  ]

  provisioner "ansible" {
    playbook_file = "ansible/jenkins-controller/playbook.yml"
    ansible_env_vars = [
      "AWS_REGION=${var.AWS_REGION}",
      "jenkins_worker_ami_id=${var.jenkins_worker_ami_id}",
    ]
  }
}
