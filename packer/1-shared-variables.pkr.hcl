variable "AWS_REGION" {
  description = "Environment variable that needs to be set; the region to deploy on"
  type        = string
}
