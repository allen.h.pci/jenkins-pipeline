variable "jenkins_worker_ami_build_uuid" {
  description = "Environment variable that needs to be set; the random unique id used to tag the worker AMI"
  type        = string
  default     = ""
}

source "amazon-ebs" "jenkins-worker" {
  ami_name      = "jenkins-worker-${formatdate("YYYYMMDD-hhmmss", timestamp())}"
  instance_type = "t3.large"
  region        = "${var.AWS_REGION}"
  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm*gp2"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  ssh_username = "ec2-user"

  tags = {
    build_uuid = "${var.jenkins_worker_ami_build_uuid}"
  }
}

build {
  name = "jenkins-worker-build"
  sources = [
    "source.amazon-ebs.jenkins-worker"
  ]

  provisioner "ansible" {
    playbook_file = "ansible/jenkins-worker/playbook.yml"
  }
}
