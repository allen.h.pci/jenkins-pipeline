variable "AWS_REGION" {
  description = "Environment variable that needs to be set; the region to deploy on"
  type        = string
}

variable "TEAM_IP_ADDRESSES" {
  description = "Environment variable that needs to be set; a list of the team member's IP addresses"
  sensitive   = true
  type        = list(string)
}
