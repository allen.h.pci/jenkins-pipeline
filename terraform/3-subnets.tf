resource "aws_subnet" "pipeline-public-sn" {
  vpc_id     = aws_vpc.pipeline-vpc.id
  cidr_block = "192.168.2.0/24"

  tags = {
    Name = "pipeline-public-sn"
  }
}

resource "aws_subnet" "pipeline-private-sn" {
  vpc_id     = aws_vpc.pipeline-vpc.id
  cidr_block = "192.168.1.0/24"

  tags = {
    Name = "pipeline-private-sn"
  }
}
