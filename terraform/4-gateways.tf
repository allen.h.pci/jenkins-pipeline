resource "aws_internet_gateway" "pipeline-gw" {
  vpc_id = aws_vpc.pipeline-vpc.id

  tags = {
    Name = "pipeline-gw"
  }
}

resource "aws_eip" "pipeline-ngw-eip" {
  vpc              = true
  public_ipv4_pool = "amazon"
}

resource "aws_nat_gateway" "pipeline-ngw" {
  connectivity_type = "public"
  allocation_id     = aws_eip.pipeline-ngw-eip.id
  subnet_id         = aws_subnet.pipeline-private-sn.id

  tags = {
    Name = "pipeline-ngw"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.pipeline-gw]
}
