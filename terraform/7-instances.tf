resource "aws_iam_role" "ssm-iam-role" {
  name = "ssm-iam-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    Name = "Role for Systems Manager permission"
  }
}

resource "aws_iam_policy_attachment" "ssm-iam-policy-attach" {
  name       = "ssm-iam-policy-attach"
  roles      = [aws_iam_role.ssm-iam-role.id]
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_instance_profile" "ssm-iam-profile" {
  name = "ssm-iam-profile"
  role = aws_iam_role.ssm-iam-role.id
}

data "aws_ami" "jenkins-controller-ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["jenkins-controller-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "pipeline-jenkins-control-ec2" {
  ami                         = data.aws_ami.jenkins-controller-ami.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.pipeline-public-sn.id
  vpc_security_group_ids      = [aws_security_group.pipeline-public-sg.id]
  associate_public_ip_address = true
  iam_instance_profile = aws_iam_instance_profile.ssm-iam-profile.id

  tags = {
    Name = "pipeline-jenkins-control-ec2"
  }
}

# data "aws_ami" "jenkins-worker-ami" {
#   most_recent = true
#   owners      = ["self"]

#   filter {
#     name   = "name"
#     values = ["jenkins-worker-*"]
#   }

#   filter {
#     name   = "root-device-type"
#     values = ["ebs"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
# }

# resource "aws_instance" "pipeline-jenkins-worker-ec2" {
#   ami                    = data.aws_ami.jenkins-worker-ami.id
#   instance_type          = "t3.large"
#   subnet_id              = aws_subnet.pipeline-private-sn.id
#   vpc_security_group_ids = [aws_security_group.pipeline-private-sg.id]
#   iam_instance_profile = aws_iam_instance_profile.ssm-iam-profile.id

#   tags = {
#     Name = "pipeline-jenkins-worker-ec2"
#   }
# }

# resource "aws_instance" "pipeline-harbor-ec2" {
#   ami                    = data.aws_ami.al2-x86-ami.id
#   instance_type          = "t3.large"
#   subnet_id              = aws_subnet.pipeline-private-sn.id
#   vpc_security_group_ids = [aws_security_group.pipeline-private-sg.id]
#   iam_instance_profile = aws_iam_instance_profile.ssm-iam-profile.id

#   tags = {
#     Name = "pipeline-harbor-ec2"
#   }
# }

# resource "aws_ebs_volume" "pipeline-harbor-ebs" {
#   availability_zone = aws_instance.pipeline-harbor-ec2.availability_zone
#   size              = 160

#   tags = {
#     Name = "pipeline-harbor-ebs"
#   }
# }

# resource "aws_volume_attachment" "pipeline-harbor-ebsa" {
#   device_name = "/dev/sdh"
#   volume_id   = aws_ebs_volume.pipeline-harbor-ebs.id
#   instance_id = aws_instance.pipeline-harbor-ec2.id
# }
