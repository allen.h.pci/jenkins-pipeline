resource "aws_security_group" "pipeline-public-sg" {
  name        = "pipeline-public-sg"
  description = "Allow ingress from specific IPs, allow egress to anywhere"
  vpc_id      = aws_vpc.pipeline-vpc.id

  ingress = [
    {
      description      = "SSH from team"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = var.TEAM_IP_ADDRESSES
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "HTTP from team"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = var.TEAM_IP_ADDRESSES
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "HTTPS from team"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = var.TEAM_IP_ADDRESSES
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "8080/tcp from team"
      from_port        = 8080
      to_port          = 8080
      protocol         = "tcp"
      cidr_blocks      = var.TEAM_IP_ADDRESSES
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress = [
    {
      description      = "All"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  tags = {
    Name = "pipeline-public-sg"
  }
}

resource "aws_security_group" "pipeline-private-sg" {
  name        = "pipeline-private-sg"
  description = "Allow ingress from same network [22/tcp, 80/tcp, 443/tcp, 8228/tcp], allow egress to anywhere"
  vpc_id      = aws_vpc.pipeline-vpc.id

  ingress = [
    {
      description      = "SSH from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = [aws_vpc.pipeline-vpc.cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "HTTP from VPC"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = [aws_vpc.pipeline-vpc.cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "HTTPS from VPC"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = [aws_vpc.pipeline-vpc.cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "Anchore Engine from VPC"
      from_port        = 8228
      to_port          = 8228
      protocol         = "tcp"
      cidr_blocks      = [aws_vpc.pipeline-vpc.cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress = [
    {
      description      = "All"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  tags = {
    Name = "pipeline-private-sg"
  }
}
