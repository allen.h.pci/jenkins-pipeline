resource "aws_route_table" "pipeline-public-rt" {
  vpc_id = aws_vpc.pipeline-vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = aws_internet_gateway.pipeline-gw.id
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      nat_gateway_id             = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    }
  ]

  tags = {
    Name = "pipeline-public-rt"
  }
}

resource "aws_route_table" "pipeline-private-rt" {
  vpc_id = aws_vpc.pipeline-vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      nat_gateway_id             = aws_nat_gateway.pipeline-ngw.id
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    }
  ]

  tags = {
    Name = "pipeline-private-rt"
  }
}

resource "aws_route_table_association" "pipeline-public-rta" {
  subnet_id      = aws_subnet.pipeline-public-sn.id
  route_table_id = aws_route_table.pipeline-public-rt.id
}

resource "aws_route_table_association" "pipeline-private-rta" {
  subnet_id      = aws_subnet.pipeline-private-sn.id
  route_table_id = aws_route_table.pipeline-private-rt.id
}
